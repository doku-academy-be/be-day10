CREATE TABLE operator (
  id serial PRIMARY KEY,
  name varchar(255),
  address text,
  phone_number varchar(25),
  email varchar(25),
  date_of_birth date,
  gender varchar(11)
);

CREATE TABLE payment_methods (
  id serial PRIMARY KEY,
  name varchar(255),
  created_at timestamp,
  update_at timestamp
);

CREATE TABLE product_type (
  id serial PRIMARY KEY,
  name_product varchar(255),
  created_at timestamp,
  update_at timestamp
);

CREATE TABLE product_desc(
  id serial primary KEY,
  description text,
  created_at timestamp,
  update_at timestamp
);

CREATE TABLE "user" (
  id serial PRIMARY key,
  name varchar(255),
  address text,
  phone_number varchar,
  email varchar(50),
  date_of_birth date,
  gender varchar(11),
  status int,
  created_at timestamp,
  update_at timestamp
);

CREATE TABLE products(
  id serial PRIMARY KEY,
  id_product_type int,
  id_product_desc int unique not null,
  id_operator int,
  code varchar(50),
  name varchar(255),
  status int,
  created_at timestamp,
  update_at timestamp
  
);
ALTER TABLE products ADD CONSTRAINT fk_product_type FOREIGN KEY (id_product_type) REFERENCES product_type(id);
ALTER TABLE products ADD CONSTRAINT fk_product_desc FOREIGN KEY (id_product_desc) REFERENCES product_desc(id);
ALTER TABLE products ADD CONSTRAINT fk_operator FOREIGN KEY (id_operator) REFERENCES operator(id);

CREATE TABLE transaction (
  id serial PRIMARY KEY,
  id_user int,
  id_payment_methods int,
  status varchar(10),
  total_qty int,
  total_price numeric(25,2),
  created_at timestamp,
  update_at timestamp
);
ALTER TABLE transaction ADD CONSTRAINT fk_user FOREIGN KEY (id_user) REFERENCES "user"(id);
ALTER TABLE transaction ADD CONSTRAINT fk_payment_methods FOREIGN KEY (id_payment_methods) REFERENCES payment_methods(id);

CREATE TABLE transaction_details (
  id serial PRIMARY KEY,
  id_transaction int,
  id_products int,
  status varchar(10),
  qty int,
  price numeric(25,2),
  created_at timestamp,
  update_at timestamp
);
ALTER TABLE transaction_details ADD CONSTRAINT fk_transaction FOREIGN KEY (id_transaction) REFERENCES transaction(id);
ALTER TABLE transaction_details ADD CONSTRAINT fk_products FOREIGN KEY (id_products) REFERENCES products(id);


CREATE TABLE kurir(
  id serial PRIMARY KEY,
  name varchar(255),
  created_at timestamp,
  update_at timestamp
);

ALTER TABLE kurir ADD COLUMN ongkos_dasar numeric(25,2);
ALTER TABLE kurir rename to shipping;
drop table shipping;

CREATE TABLE payment_methods_description(
  id serial PRIMARY KEY,
  id_payment_methods int unique not null,
  payment_methods_desc text,
  created_at timestamp,
  update_at timestamp
);
ALTER TABLE payment_methods_description ADD CONSTRAINT fk_payment_method FOREIGN KEY (id_payment_methods) REFERENCES payment_methods(id);

create table alamat(
  id serial primary key,
  description text,
  id_user int
);
ALTER TABLE alamat ADD CONSTRAINT fk_user FOREIGN KEY (id_user) REFERENCES "user"(id);


create table user_payment_method_detail(
  id_user int,
  id_payment_methods int
);
ALTER TABLE user_payment_method_detail ADD CONSTRAINT fk_user FOREIGN KEY (id_user) REFERENCES "user"(id);
ALTER TABLE user_payment_method_detail ADD CONSTRAINT fk_payment_methods FOREIGN KEY (id_payment_methods) REFERENCES payment_methods(id);






